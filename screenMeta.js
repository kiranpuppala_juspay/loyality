const RootScreen = require("./views/RootScreen");
// Screens of page: Page 3
const VerifyMobile = require("./views/Page 3/VerifyMobile");
const Transactions = require("./views/Page 3/Transactions");
const HomeScreen = require("./views/Page 3/HomeScreen");
const RedeemScreen = require("./views/Page 3/RedeemScreen");
const OTPWaiting = require("./views/Page 3/OTPWaiting");
const A_2b1_OTP_FilledSS = require("./views/Page 3/A_2b1_OTP_FilledSS");
const OTPFilledNew = require("./views/Page 3/OTPFilledNew");
const AddVPA = require("./views/Page 3/AddVPA");
const A_3a_Add_VPA_TypingSS = require("./views/Page 3/A_3a_Add_VPA_TypingSS");
const A_3a_Add_VPA_FilledSS = require("./views/Page 3/A_3a_Add_VPA_FilledSS");

const screens = {
	VerifyMobile,
	Transactions,
	HomeScreen,
	RedeemScreen,
	OTPWaiting,
	A_2b1_OTP_FilledSS,
	OTPFilledNew,
	AddVPA,
	A_3a_Add_VPA_TypingSS,
	A_3a_Add_VPA_FilledSS,
	RootScreen
};

const INIT_UI = "VerifyMobile";

module.exports = {
	screens,
	INIT_UI
};
