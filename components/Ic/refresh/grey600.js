const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const ImageView = require("@juspay/mystique-backend").views.ImageView;

const Config = require('./../../../globalConfig');
const Controller = require('./../../../controller/components/Ic/refresh/grey600');
const Strings = require('./../../../res/strings');
const Accessibility = require('./../../../res/accessibility');

let STR = {};
let HINT = {};

class Ic_refresh_grey600 extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<ImageView
				id={this.idSet.Ic_refresh_24px}
				height={this.props.height ? this.props.height:"24"}
				width={this.props.width ? this.props.width:"24"}
				imageUrl="ic_refresh_24px11"
				orientation="vertical"
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.IC_REFRESH_GREY600_IC_REFRESH_24PX}
				style={this.style_Ic_refresh_24px} />
		);
		return this.layout.render();
	}

};

module.exports = Ic_refresh_grey600;
