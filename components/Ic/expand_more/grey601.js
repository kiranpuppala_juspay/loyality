const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const ImageView = require("@juspay/mystique-backend").views.ImageView;

const Config = require('./../../../globalConfig');
const Controller = require('./../../../controller/components/Ic/expand_more/grey601');
const Strings = require('./../../../res/strings');
const Accessibility = require('./../../../res/accessibility');

let STR = {};
let HINT = {};

class Ic_expand_more_grey601 extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<ImageView
				id={this.idSet.Ic_expand_more_24px}
				height={this.props.height ? this.props.height:"32"}
				width={this.props.width ? this.props.width:"32"}
				orientation="vertical"
				padding="8,11,8,10"
				imageUrl="ic_expand_more_24px10"
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.IC_EXPAND_MORE_GREY601_IC_EXPAND_MORE_24PX}
				style={this.style_Ic_expand_more_24px} />
		);
		return this.layout.render();
	}

};

module.exports = Ic_expand_more_grey601;
