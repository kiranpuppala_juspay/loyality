const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/components/List_Item/Debit');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class List_Item_Debit extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				id={this.idSet.List_Item_Debit}
				orientation="vertical"
				root={true}
				width={this.props.width ? this.props.width:"0"}
				height={this.props.height ? this.props.height:"0"}
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.LIST_ITEM_DEBIT_LIST_ITEM_DEBIT}
				style={this.style_List_Item_Debit} />
		);
		return this.layout.render();
	}

};

module.exports = List_Item_Debit;
