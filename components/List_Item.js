const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const TextView = require("@juspay/mystique-backend").views.TextView;

const Config = require('./../globalConfig');
const Controller = require('./../controller/components/List_Item');
const Strings = require('./../res/strings');
const Accessibility = require('./../res/accessibility');

let STR = {};
let HINT = {};

class List_Item extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				id={this.idSet.Group_4}
				height={this.props.height ? this.props.height:"72"}
				width={this.props.width ? this.props.width:"320"}
				orientation="vertical"
				gravity="center_horizontal"
				padding="0,17,0,0"
				background="#ff202296"
				cornerRadius="0"
				root={true}
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.LIST_ITEM_GROUP_4}
				style={this.style_Group_4}>
				<LinearLayout
					id={this.idSet.Group_3}
					height="38"
					width="match_parent"
					orientation="horizontal"
					gravity="center_vertical"
					margin="16,0,16,0"
					accessibilityHint={HINT.LIST_ITEM_GROUP_3}
					style={this.style_Group_3}>
					<LinearLayout
						id={this.idSet.Group_2}
						height="38"
						width="190"
						orientation="vertical"
						gravity="center_horizontal"
						weight="1"
						accessibilityHint={HINT.LIST_ITEM_GROUP_2}
						style={this.style_Group_2}>
						<TextView
							id={this.idSet.MG_Road_Station}
							height="20"
							width="match_parent"
							textSize="16"
							color="#ffffffff"
							fontStyle="SourceSansPro-Regular"
							gravity="left"
							accessibilityHint={HINT.LIST_ITEM_MG_ROAD_STATION}
							text={STR.LIST_ITEM_MG_ROAD_STATION}
							style={this.style_MG_Road_Station} />
						<TextView
							id={this.idSet.Sahil_upi}
							height="18"
							width="match_parent"
							textSize="14"
							color="#ffffffff"
							fontStyle="SourceSansPro-Regular"
							gravity="left"
							accessibilityHint={HINT.LIST_ITEM_SAHIL_UPI}
							text={STR.LIST_ITEM_SAHIL_UPI}
							style={this.style_Sahil_upi} />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.Group}
						height="32"
						width="49"
						orientation="horizontal"
						margin="49,0,0,0"
						background="#00765151"
						cornerRadius="0"
						accessibilityHint={HINT.LIST_ITEM_GROUP}
						style={this.style_Group}>
						<TextView
							id={this.idSet.A__30}
							height="31"
							width="44"
							margin="0,1,0,0"
							textSize="24"
							color="#ffffffff"
							fontStyle="SourceCodePro-Medium"
							gravity="right"
							accessibilityHint={HINT.LIST_ITEM_A__30}
							text={STR.LIST_ITEM_A__30}
							style={this.style_A__30} />
						<TextView
							id={this.idSet.P}
							height="10"
							width="5"
							textSize="8"
							color="#ffffffff"
							fontStyle="SourceCodePro-Medium"
							gravity="left"
							accessibilityHint={HINT.LIST_ITEM_P}
							text={STR.LIST_ITEM_P}
							style={this.style_P} />
					</LinearLayout>
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Rectangle_2}
					height="1"
					width="match_parent"
					background="#54ffffff"
					margin="0,16,0,0"
					cornerRadius="0"
					accessibilityHint={HINT.LIST_ITEM_RECTANGLE_2}
					style={this.style_Rectangle_2} />
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = List_Item;
