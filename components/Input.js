const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const EditText = require("@juspay/mystique-backend").views.EditText;

const Config = require('./../globalConfig');
const Controller = require('./../controller/components/Input');
const Strings = require('./../res/strings');
const Accessibility = require('./../res/accessibility');

let STR = {};
let HINT = {};

class Input extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
		this.InputText = STR.DEFAULT_INPUT_INPUTTEXT;
	}

	onPop = () => {}

	render = () => {
		let InputText = (this.props.InputText) ? this.props.InputText : this.InputText;
		this.layout = (
			<LinearLayout
				id={this.idSet.InputGroup}
				height={this.props.height ? this.props.height:"68"}
				width={this.props.width ? this.props.width:"match_parent"}
				orientation="vertical"
				padding="20,18,20,19"
				background="#fff4f4fa"
				cornerRadius="0"
				root={true}
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.INPUT_INPUTGROUP}
				style={this.style_InputGroup}>
				<EditText
					id={this.idSet.InputText}
					height="31"
					width="match_parent"
					textSize="24"
					color="#ff1f2196"
					fontStyle="SourceSansPro-Regular"
					gravity="left"
					hint={InputText}
					background="#fff4f4fa"
					padding="0,0,0,0"
					accessibilityHint={HINT.INPUT_INPUTTEXT}
					style={this.style_InputText} />
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = Input;
