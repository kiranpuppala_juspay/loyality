const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const ImageView = require("@juspay/mystique-backend").views.ImageView;
const TextView = require("@juspay/mystique-backend").views.TextView;

const Config = require('./../globalConfig');
const Controller = require('./../controller/components/HeaderWithArrow');
const Strings = require('./../res/strings');
const Accessibility = require('./../res/accessibility');

let STR = {};
let HINT = {};

class HeaderWithArrow extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				id={this.idSet.Header}
				height={this.props.height ? this.props.height:"64"}
				width={this.props.width ? this.props.width:"360"}
				orientation="horizontal"
				gravity="center_vertical"
				padding="4,8,20,8"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.HEADERWITHARROW_HEADER}
				style={this.style_Header}>
				<ImageView
					id={this.idSet.Icon}
					height="48"
					width="48"
					orientation="vertical"
					padding="16,16,16,16"
					imageUrl="icon13"
					accessibilityHint={HINT.HEADERWITHARROW_ICON}
					style={this.style_Icon} />
				<TextView
					id={this.idSet.HeaderText}
					height="28"
					width="284"
					margin="4,0,0,0"
					weight="1"
					textSize="22"
					color="#ff333333"
					fontStyle="SourceSansPro-Bold"
					gravity="left"
					accessibilityHint={HINT.HEADERWITHARROW_HEADERTEXT}
					text={STR.HEADERWITHARROW_HEADERTEXT}
					style={this.style_HeaderText} />
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = HeaderWithArrow;
