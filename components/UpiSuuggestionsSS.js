const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const TextView = require("@juspay/mystique-backend").views.TextView;

const Config = require('./../globalConfig');
const Controller = require('./../controller/components/UpiSuuggestionsSS');
const Strings = require('./../res/strings');
const Accessibility = require('./../res/accessibility');

let STR = {};
let HINT = {};

class UpiSuuggestionsSS extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				id={this.idSet.UpiSuuggestions}
				height={this.props.height ? this.props.height:"34"}
				width={this.props.width ? this.props.width:"320"}
				orientation="horizontal"
				gravity="center_vertical"
				padding="0,5,0,5"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.UPISUUGGESTIONSSS_UPISUUGGESTIONS}
				style={this.style_UpiSuuggestions}>
				<TextView
					id={this.idSet.Priyakum_upi}
					height="20"
					width="296"
					weight="1"
					textSize="16"
					color="#ff333333"
					fontStyle="SourceSansPro-Regular"
					gravity="left"
					accessibilityHint={HINT.UPISUUGGESTIONSSS_PRIYAKUM_UPI}
					text={STR.UPISUUGGESTIONSSS_PRIYAKUM_UPI}
					style={this.style_Priyakum_upi} />
				<LinearLayout
					id={this.idSet.UPI_Logo}
					height="24"
					width="24"
					accessibilityHint={HINT.UPISUUGGESTIONSSS_UPI_LOGO}
					style={this.style_UPI_Logo}>
					<LinearLayout
						id={this.idSet.Rectangle_7}
						height="24"
						width="24"
						background="#ffffffff"
						stroke="0,#d3d3ea"
						cornerRadius="2"
						accessibilityHint={HINT.UPISUUGGESTIONSSS_RECTANGLE_7}
						style={this.style_Rectangle_7} />
					<LinearLayout
						id={this.idSet.Shape}
						height="15"
						width="8"
						background="#ff008c3b"
						cornerRadius="0"
						accessibilityHint={HINT.UPISUUGGESTIONSSS_SHAPE}
						style={this.style_Shape} />
					<LinearLayout
						id={this.idSet.Shape}
						height="15"
						width="8"
						background="#ffff7907"
						cornerRadius="0"
						accessibilityHint={HINT.UPISUUGGESTIONSSS_SHAPE}
						style={this.style_Shape} />
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = UpiSuuggestionsSS;
