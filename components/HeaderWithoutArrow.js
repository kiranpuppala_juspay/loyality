const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const TextView = require("@juspay/mystique-backend").views.TextView;

const Config = require('./../globalConfig');
const Controller = require('./../controller/components/HeaderWithoutArrow');
const Strings = require('./../res/strings');
const Accessibility = require('./../res/accessibility');

let STR = {};
let HINT = {};

class HeaderWithoutArrow extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
		this.HeaderText = STR.DEFAULT_HEADERWITHOUTARROW_HEADERTEXT;
	}

	onPop = () => {}

	render = () => {
		let HeaderText = (this.props.HeaderText) ? this.props.HeaderText : this.HeaderText;
		this.layout = (
			<LinearLayout
				id={this.idSet.HeaderGroup}
				height={this.props.height ? this.props.height:"64"}
				width={this.props.width ? this.props.width:"360"}
				orientation="vertical"
				padding="20,18,20,18"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.HEADERWITHOUTARROW_HEADERGROUP}
				style={this.style_HeaderGroup}>
				<TextView
					id={this.idSet.HeaderText}
					height="28"
					width="320"
					text={HeaderText}
					textSize="22"
					color="#ff333333"
					fontStyle="SourceSansPro-Bold"
					gravity="left"
					accessibilityHint={HINT.HEADERWITHOUTARROW_HEADERTEXT}
					style={this.style_HeaderText} />
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = HeaderWithoutArrow;
