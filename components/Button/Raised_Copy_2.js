const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const TextView = require("@juspay/mystique-backend").views.TextView;
const ImageView = require("@juspay/mystique-backend").views.ImageView;

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/components/Button/Raised_Copy_2');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class Button_Raised_Copy_2 extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				id={this.idSet.Group}
				height={this.props.height ? this.props.height:"36"}
				width={this.props.width ? this.props.width:"206"}
				orientation="horizontal"
				gravity="center_vertical"
				padding="8,6,8,6"
				background="#ff202296"
				cornerRadius="0"
				root={true}
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.BUTTON_RAISED_COPY_2_GROUP}
				style={this.style_Group}>
				<TextView
					id={this.idSet.VIEW_DETAILED_STATEM}
					height="15"
					width="141"
					weight="1"
					textSize="12"
					color="#ffffffff"
					fontStyle="SourceSansPro-Regular"
					gravity="right"
					accessibilityHint={HINT.BUTTON_RAISED_COPY_2_VIEW_DETAILED_STATEM}
					text={STR.BUTTON_RAISED_COPY_2_VIEW_DETAILED_STATEM}
					style={this.style_VIEW_DETAILED_STATEM} />
				<ImageView
					id={this.idSet.Detailed}
					height="24"
					width="24"
					orientation="vertical"
					padding="8,6,8,6"
					margin="25,0,0,0"
					imageUrl="detailed12"
					accessibilityHint={HINT.BUTTON_RAISED_COPY_2_DETAILED}
					style={this.style_Detailed} />
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = Button_Raised_Copy_2;
