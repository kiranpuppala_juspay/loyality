const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const TextView = require("@juspay/mystique-backend").views.TextView;

const Config = require('./../globalConfig');
const Controller = require('./../controller/components/PrimaryButton');
const Strings = require('./../res/strings');
const Accessibility = require('./../res/accessibility');

let STR = {};
let HINT = {};

class PrimaryButton extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
		this.ButtonText = STR.DEFAULT_PRIMARYBUTTON_BUTTONTEXT;
	}

	onPop = () => {}

	render = () => {
		let ButtonText = (this.props.ButtonText) ? this.props.ButtonText : this.ButtonText;
		this.layout = (
			<LinearLayout
				id={this.idSet.ButtonGroup}
				height={this.props.height ? this.props.height:"48"}
				width={this.props.width ? this.props.width:"344"}
				orientation="vertical"
				padding="0,14,0,16"
				background="#ff202296"
				cornerRadius="4"
				root={true}
				margin={this.props.margin ? this.props.margin:"0,0,0,0"}
				weight={this.props.weight ? this.props.weight:"0"}
				accessibilityHint={HINT.PRIMARYBUTTON_BUTTONGROUP}
				style={this.style_ButtonGroup}>
				<TextView
					id={this.idSet.ButtonText}
					height="18"
					width="344"
					text={ButtonText}
					textSize="14"
					color="#ffffffff"
					fontStyle="SourceSansPro-Semibold"
					gravity="center"
					accessibilityHint={HINT.PRIMARYBUTTON_BUTTONTEXT}
					style={this.style_ButtonText} />
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = PrimaryButton;
