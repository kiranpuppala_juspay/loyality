const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const HeaderWithoutArrow = require('../../components/HeaderWithoutArrow');
const ScrollView = require("@juspay/mystique-backend").views.ScrollView;
const TextView = require("@juspay/mystique-backend").views.TextView;
const PrimaryButton = require('../../components/PrimaryButton');

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/OTPFilledNew');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class OTPFilledNew extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				gravity="center_horizontal"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.OTPFILLEDNEW_OTPFILLEDNEW}
				style={this.style_OTPFilledNew}>
				<HeaderWithoutArrow
					parentProps={this.props_Header}
					height="64"
					width="match_parent"
					HeaderText={STR.OTPFILLEDNEW_HEADER_HEADERTEXT} />
				<ScrollView
					id={this.idSet.OTPFilledScrollView}
					height="0"
					width="match_parent"
					weight="1"
					background="#ffffffff"
					cornerRadius="0"
					accessibilityHint={HINT.OTPFILLEDNEW_OTPFILLEDSCROLLVIEW}
					style={this.style_OTPFilledScrollView}>
					<LinearLayout
						id={this.idSet.OTPFilledGroup}
						height="152"
						width="360"
						orientation="vertical"
						gravity="center_horizontal"
						padding="0,20,0,0"
						background="#ffffffff"
						cornerRadius="0"
						accessibilityHint={HINT.OTPFILLEDNEW_OTPFILLEDGROUP}
						style={this.style_OTPFilledGroup}>
						<TextView
							id={this.idSet.ParaText}
							height="44"
							width="match_parent"
							margin="20,0,20,0"
							textSize="16"
							color="#ff333333"
							fontStyle="SourceSansPro-Regular"
							lineHeight="22px"
							gravity="left"
							accessibilityHint={HINT.OTPFILLEDNEW_PARATEXT}
							text={STR.OTPFILLEDNEW_PARATEXT}
							style={this.style_ParaText} />
						<LinearLayout
							id={this.idSet.OTPGroup}
							height="68"
							width="match_parent"
							orientation="vertical"
							padding="0,18,0,19"
							margin="0,20,0,0"
							background="#fff4f4fa"
							cornerRadius="0"
							accessibilityHint={HINT.OTPFILLEDNEW_OTPGROUP}
							style={this.style_OTPGroup}>
							<TextView
								id={this.idSet.OTPText}
								height="31"
								width="360"
								textSize="24"
								color="#ff1f2196"
								fontStyle="SourceSansPro-Semibold"
								gravity="center"
								accessibilityHint={HINT.OTPFILLEDNEW_OTPTEXT}
								text={STR.OTPFILLEDNEW_OTPTEXT}
								style={this.style_OTPText} />
						</LinearLayout>
					</LinearLayout>
				</ScrollView>
				<LinearLayout
					id={this.idSet.ButtonGroup}
					height="64"
					width="match_parent"
					orientation="vertical"
					padding="8,8,8,8"
					background="#ffffffff"
					cornerRadius="0"
					accessibilityHint={HINT.OTPFILLEDNEW_BUTTONGROUP}
					style={this.style_ButtonGroup}>
					<PrimaryButton
						parentProps={this.props_PrimaryButton}
						height="48"
						width="match_parent"
						ButtonText={STR.OTPFILLEDNEW_PRIMARYBUTTON_BUTTONTEXT} />
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = OTPFilledNew;
