const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const HeaderWithArrow = require('../../components/HeaderWithArrow');
const TextView = require("@juspay/mystique-backend").views.TextView;
const Input = require('../../components/Input');
const UpiSuuggestionsSS = require('../../components/UpiSuuggestionsSS');

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/A_3a_Add_VPA_TypingSS');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class A_3a_Add_VPA_TypingSS extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				gravity="center_horizontal"
				padding="0,0,0,305"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.A_3A_ADD_VPA_TYPINGSS_GROUP}
				style={this.style_Group}>
				<HeaderWithArrow
					parentProps={this.props_HeaderNav}
					height="64"
					width="match_parent" />
				<TextView
					id={this.idSet.You_d_be_rewarded_po}
					height="44"
					width="match_parent"
					margin="20,10,20,0"
					textSize="16"
					color="#ff333333"
					fontStyle="SourceSansPro-Regular"
					lineHeight="22px"
					gravity="left"
					accessibilityHint={HINT.A_3A_ADD_VPA_TYPINGSS_YOU_D_BE_REWARDED_PO}
					text={STR.A_3A_ADD_VPA_TYPINGSS_YOU_D_BE_REWARDED_PO}
					style={this.style_You_d_be_rewarded_po} />
				<Input
					parentProps={this.props_Input}
					height="68"
					width="match_parent"
					margin="0,13,0,0"
					InputText={STR.A_3A_ADD_VPA_TYPINGSS_INPUT_INPUTTEXT} />
				<TextView
					id={this.idSet.SUGGESTIONS}
					height="15"
					width="match_parent"
					margin="20,14,20,0"
					textSize="12"
					color="#ff333333"
					fontStyle="SourceSansPro-Semibold"
					gravity="left"
					accessibilityHint={HINT.A_3A_ADD_VPA_TYPINGSS_SUGGESTIONS}
					text={STR.A_3A_ADD_VPA_TYPINGSS_SUGGESTIONS}
					style={this.style_SUGGESTIONS} />
				<LinearLayout
					id={this.idSet.Group_2}
					height="102"
					width="match_parent"
					orientation="vertical"
					gravity="center_horizontal"
					margin="20,5,20,0"
					accessibilityHint={HINT.A_3A_ADD_VPA_TYPINGSS_GROUP_2}
					style={this.style_Group_2}>
					<UpiSuuggestionsSS
						parentProps={this.props_UpiSuuggestions}
						height="34"
						width="match_parent" />
					<UpiSuuggestionsSS
						parentProps={this.props_UpiSuuggestions}
						height="34"
						width="match_parent" />
					<UpiSuuggestionsSS
						parentProps={this.props_UpiSuuggestions}
						height="34"
						width="match_parent" />
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = A_3a_Add_VPA_TypingSS;
