const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const ImageView = require("@juspay/mystique-backend").views.ImageView;
const TextView = require("@juspay/mystique-backend").views.TextView;
const Ic_expand_more_grey601 = require('../../components/Ic/expand_more/grey601');

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/RedeemScreen');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class RedeemScreen extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	Ic_expand_more_grey600_flow = () => {
		window.__duiShowScreen(null, {screen: "Transactions"});
	}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				padding="20,20,20,52"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.REDEEMSCREEN_REDEEMSCREEN}
				style={this.style_RedeemScreen}>
				<LinearLayout
					id={this.idSet.Header}
					height="48"
					width="match_parent"
					orientation="horizontal"
					gravity="center_vertical"
					background="#00ffffff"
					cornerRadius="0"
					accessibilityHint={HINT.REDEEMSCREEN_HEADER}
					style={this.style_Header}>
					<ImageView
						id={this.idSet.Scroll}
						height="48"
						width="16"
						orientation="vertical"
						gravity="center_horizontal"
						padding="0,4,0,0"
						imageUrl="scroll7"
						accessibilityHint={HINT.REDEEMSCREEN_SCROLL}
						style={this.style_Scroll} />
					<LinearLayout
						id={this.idSet.Space}
						weight="1"
						accessibilityHint={HINT.REDEEMSCREEN_SPACE}
						style={this.style_Space} />
					<ImageView
						id={this.idSet.Group_9}
						height="48"
						width="48"
						orientation="vertical"
						imageUrl="group98"
						accessibilityHint={HINT.REDEEMSCREEN_GROUP_9}
						style={this.style_Group_9} />
				</LinearLayout>
				<LinearLayout
					id={this.idSet.RedeemGroup}
					height="330"
					width="match_parent"
					orientation="vertical"
					gravity="center_horizontal"
					padding="16,21,16,278"
					margin="0,87,0,0"
					background="#ff202296"
					cornerRadius="0"
					accessibilityHint={HINT.REDEEMSCREEN_REDEEMGROUP}
					style={this.style_RedeemGroup}>
					<TextView
						id={this.idSet.HowToText}
						height="31"
						width="288"
						textSize="24"
						color="#ffffffff"
						fontStyle="SourceSansPro-Regular"
						gravity="left"
						accessibilityHint={HINT.REDEEMSCREEN_HOWTOTEXT}
						text={STR.REDEEMSCREEN_HOWTOTEXT}
						style={this.style_HowToText} />
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Space}
					weight="1"
					accessibilityHint={HINT.REDEEMSCREEN_SPACE}
					style={this.style_Space} />
				<LinearLayout
					id={this.idSet.ViewStatGroup}
					height="32"
					width="163"
					orientation="horizontal"
					gravity="center_vertical"
					accessibilityHint={HINT.REDEEMSCREEN_VIEWSTATGROUP}
					style={this.style_ViewStatGroup}>
					<Ic_expand_more_grey601
						parentProps={this.props_Ic_expand_more_grey600}
						height="32"
						width="32"
						orientation="horizontal"
						gravity="center_vertical"
						onClick={this.Ic_expand_more_grey600_flow} />
					<TextView
						id={this.idSet.View_Statement}
						height="16"
						width="126"
						margin="4,0,0,0"
						textSize="14"
						color="#ff333333"
						fontStyle="Roboto-Medium"
						gravity="left"
						accessibilityHint={HINT.REDEEMSCREEN_VIEW_STATEMENT}
						text={STR.REDEEMSCREEN_VIEW_STATEMENT}
						style={this.style_View_Statement} />
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = RedeemScreen;
