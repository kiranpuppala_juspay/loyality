const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const HeaderWithArrow = require('../../components/HeaderWithArrow');
const ScrollView = require("@juspay/mystique-backend").views.ScrollView;
const TextView = require("@juspay/mystique-backend").views.TextView;
const Input = require('../../components/Input');
const PrimaryButton = require('../../components/PrimaryButton');

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/AddVPA');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class AddVPA extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				gravity="center_horizontal"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.ADDVPA_ADDVPA}
				style={this.style_AddVPA}>
				<HeaderWithArrow
					parentProps={this.props_HeaderWithArrow}
					height="64"
					width="match_parent" />
				<ScrollView
					id={this.idSet.AddVPAScrollview}
					height="0"
					width="match_parent"
					weight="1"
					background="#ffffffff"
					cornerRadius="0"
					accessibilityHint={HINT.ADDVPA_ADDVPASCROLLVIEW}
					style={this.style_AddVPAScrollview}>
					<LinearLayout
						id={this.idSet.AddVPAGroup}
						height="179"
						width="360"
						orientation="vertical"
						gravity="center_horizontal"
						background="#ffffffff"
						cornerRadius="0"
						accessibilityHint={HINT.ADDVPA_ADDVPAGROUP}
						style={this.style_AddVPAGroup}>
						<TextView
							id={this.idSet.ParaText}
							height="44"
							width="match_parent"
							margin="20,0,20,0"
							textSize="16"
							color="#ff333333"
							fontStyle="SourceSansPro-Regular"
							lineHeight="22px"
							gravity="left"
							accessibilityHint={HINT.ADDVPA_PARATEXT}
							text={STR.ADDVPA_PARATEXT}
							style={this.style_ParaText} />
						<Input
							parentProps={this.props_Input}
							height="68"
							width="match_parent"
							margin="0,13,0,0"
							InputText={STR.ADDVPA_INPUT_INPUTTEXT} />
						<LinearLayout
							id={this.idSet.HelpGroup}
							height="48"
							width="match_parent"
							orientation="horizontal"
							gravity="center_vertical"
							margin="20,6,20,0"
							background="#ffffffff"
							cornerRadius="0"
							accessibilityHint={HINT.ADDVPA_HELPGROUP}
							style={this.style_HelpGroup}>
							<LinearLayout
								id={this.idSet.Indent}
								height="48"
								width="2"
								background="#ffd7d7d7"
								cornerRadius="0"
								accessibilityHint={HINT.ADDVPA_INDENT}
								style={this.style_Indent} />
							<TextView
								id={this.idSet.HelpText}
								height="30"
								width="308"
								margin="10,0,0,0"
								weight="1"
								textSize="12"
								color="#ff333333"
								fontStyle="SourceSansPro-Regular"
								gravity="left"
								accessibilityHint={HINT.ADDVPA_HELPTEXT}
								text={STR.ADDVPA_HELPTEXT}
								style={this.style_HelpText} />
						</LinearLayout>
					</LinearLayout>
				</ScrollView>
				<LinearLayout
					id={this.idSet.ButtonGroup}
					height="64"
					width="match_parent"
					orientation="vertical"
					padding="8,8,8,8"
					background="#ffffffff"
					cornerRadius="0"
					accessibilityHint={HINT.ADDVPA_BUTTONGROUP}
					style={this.style_ButtonGroup}>
					<PrimaryButton
						parentProps={this.props_PrimaryButton}
						height="48"
						width="match_parent"
						ButtonText={STR.ADDVPA_PRIMARYBUTTON_BUTTONTEXT} />
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = AddVPA;
