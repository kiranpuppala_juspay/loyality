const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const ImageView = require("@juspay/mystique-backend").views.ImageView;
const TextView = require("@juspay/mystique-backend").views.TextView;
const ScrollView = require("@juspay/mystique-backend").views.ScrollView;
const Ic_expand_more_grey601 = require('../../components/Ic/expand_more/grey601');
const Ic_expand_more_grey600 = require('../../components/Ic/expand_more/grey600');
const List_Item = require('../../components/List_Item');

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/Transactions');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class Transactions extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
		let ids = ["ScrollView_scroll"];
		this.setIds(ids);
	}

	onPop = () => {}

	List_ItemItem = (data) => {
		let layout = (
			<List_Item
				parentProps={this.props_List1}
				height="72"
				width="match_parent" />
		);
		return layout;
	}

	ScrollView_scroll = () => {
		let data = [
			{},
			{},
			{},
			{},
			{},
			{},
		];
		data.forEach((d, i)=>{
			let view = this.List_ItemItem(d).render();
			this.appendChild(this.idSet.ScrollView_scroll, view, i, null, false);
		});
	}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				padding="20,20,20,20"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.TRANSACTIONS_TRANSACTIONS}
				style={this.style_Transactions}>
				<LinearLayout
					id={this.idSet.Header}
					height="48"
					width="match_parent"
					orientation="horizontal"
					gravity="center_vertical"
					background="#00ffffff"
					cornerRadius="0"
					accessibilityHint={HINT.TRANSACTIONS_HEADER}
					style={this.style_Header}>
					<ImageView
						id={this.idSet.Scroll}
						height="48"
						width="16"
						orientation="vertical"
						gravity="center_horizontal"
						padding="0,4,0,4"
						imageUrl="scroll1"
						accessibilityHint={HINT.TRANSACTIONS_SCROLL}
						style={this.style_Scroll} />
					<LinearLayout
						id={this.idSet.Space}
						weight="1"
						accessibilityHint={HINT.TRANSACTIONS_SPACE}
						style={this.style_Space} />
					<ImageView
						id={this.idSet.Group_9}
						height="48"
						width="48"
						orientation="vertical"
						imageUrl="group92"
						accessibilityHint={HINT.TRANSACTIONS_GROUP_9}
						style={this.style_Group_9} />
				</LinearLayout>
				<LinearLayout
					id={this.idSet.RecentGroup}
					height="330"
					width="match_parent"
					orientation="vertical"
					gravity="center_horizontal"
					padding="0,21,0,16"
					margin="0,87,0,0"
					background="#ff202296"
					cornerRadius="0"
					accessibilityHint={HINT.TRANSACTIONS_RECENTGROUP}
					style={this.style_RecentGroup}>
					<TextView
						id={this.idSet.RecentText}
						height="31"
						width="match_parent"
						margin="16,0,16,0"
						textSize="24"
						color="#ffffffff"
						fontStyle="SourceSansPro-Regular"
						gravity="left"
						accessibilityHint={HINT.TRANSACTIONS_RECENTTEXT}
						text={STR.TRANSACTIONS_RECENTTEXT}
						style={this.style_RecentText} />
					<ScrollView
						id={this.idSet.ScrollView}
						height="0"
						width="match_parent"
						margin="0,6,0,0"
						weight="1"
						background="#00ffffff"
						cornerRadius="0"
						accessibilityHint={HINT.TRANSACTIONS_SCROLLVIEW}
						style={this.style_ScrollView}>
						<LinearLayout
							id={this.idSet.ScrollView_scroll}
							height="432"
							width="320"
							orientation="vertical"
							gravity="center_horizontal"
							afterRender={this.ScrollView_scroll}
							accessibilityHint={HINT.TRANSACTIONS_SCROLLGROUP}
							style={this.style_ScrollGroup} />
					</ScrollView>
					<LinearLayout
						id={this.idSet.DetailedGroup}
						height="24"
						width="match_parent"
						orientation="horizontal"
						gravity="center_vertical"
						padding="137,0,0,0"
						margin="0,16,0,0"
						background="#00ffffff"
						cornerRadius="0"
						accessibilityHint={HINT.TRANSACTIONS_DETAILEDGROUP}
						style={this.style_DetailedGroup}>
						<LinearLayout
							id={this.idSet.Space}
							weight="1"
							accessibilityHint={HINT.TRANSACTIONS_SPACE}
							style={this.style_Space} />
						<LinearLayout
							id={this.idSet.Group}
							height="24"
							width="183"
							orientation="horizontal"
							gravity="center_vertical"
							padding="0,0,8,0"
							background="#00ffffff"
							cornerRadius="0"
							accessibilityHint={HINT.TRANSACTIONS_GROUP}
							style={this.style_Group}>
							<TextView
								id={this.idSet.ViewDetailedText}
								height="15"
								width="141"
								weight="1"
								textSize="12"
								color="#ffffffff"
								fontStyle="SourceSansPro-Regular"
								gravity="right"
								accessibilityHint={HINT.TRANSACTIONS_VIEWDETAILEDTEXT}
								text={STR.TRANSACTIONS_VIEWDETAILEDTEXT}
								style={this.style_ViewDetailedText} />
							<ImageView
								id={this.idSet.DetailedIcon}
								height="24"
								width="24"
								orientation="vertical"
								padding="8,6,8,6"
								margin="10,0,0,0"
								imageUrl="detailedicon0"
								accessibilityHint={HINT.TRANSACTIONS_DETAILEDICON}
								style={this.style_DetailedIcon} />
						</LinearLayout>
					</LinearLayout>
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Space}
					weight="1"
					accessibilityHint={HINT.TRANSACTIONS_SPACE}
					style={this.style_Space} />
				<LinearLayout
					id={this.idSet.NavGroup}
					height="64"
					width="163"
					orientation="vertical"
					gravity="center_horizontal"
					accessibilityHint={HINT.TRANSACTIONS_NAVGROUP}
					style={this.style_NavGroup}>
					<LinearLayout
						id={this.idSet.ToHomeGroup}
						height="32"
						width="match_parent"
						orientation="horizontal"
						gravity="center_vertical"
						accessibilityHint={HINT.TRANSACTIONS_TOHOMEGROUP}
						style={this.style_ToHomeGroup}>
						<Ic_expand_more_grey601
							parentProps={this.props_Ic_expand_more_grey600}
							height="32"
							width="32" />
						<TextView
							id={this.idSet.HomeText}
							height="16"
							width="126"
							margin="4,0,0,0"
							textSize="14"
							color="#ff333333"
							fontStyle="Roboto-Medium"
							gravity="left"
							accessibilityHint={HINT.TRANSACTIONS_HOMETEXT}
							text={STR.TRANSACTIONS_HOMETEXT}
							style={this.style_HomeText} />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.ToHowToGroup}
						height="32"
						width="match_parent"
						orientation="horizontal"
						gravity="center_vertical"
						accessibilityHint={HINT.TRANSACTIONS_TOHOWTOGROUP}
						style={this.style_ToHowToGroup}>
						<Ic_expand_more_grey600
							parentProps={this.props_Ic_expand_more_grey600}
							height="32"
							width="32"
							orientation="horizontal"
							gravity="center_vertical" />
						<TextView
							id={this.idSet.HowtoText}
							height="16"
							width="126"
							margin="4,0,0,0"
							textSize="14"
							color="#ff333333"
							fontStyle="Roboto-Medium"
							gravity="left"
							accessibilityHint={HINT.TRANSACTIONS_HOWTOTEXT}
							text={STR.TRANSACTIONS_HOWTOTEXT}
							style={this.style_HowtoText} />
					</LinearLayout>
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = Transactions;
