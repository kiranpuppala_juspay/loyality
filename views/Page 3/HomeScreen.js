const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const ImageView = require("@juspay/mystique-backend").views.ImageView;
const TextView = require("@juspay/mystique-backend").views.TextView;
const Ic_refresh_grey600 = require('../../components/Ic/refresh/grey600');
const Ic_expand_more_grey600 = require('../../components/Ic/expand_more/grey600');

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/HomeScreen');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class HomeScreen extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				padding="20,20,20,20"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.HOMESCREEN_HOMESCREEN}
				style={this.style_HomeScreen}>
				<LinearLayout
					id={this.idSet.Header}
					height="48"
					width="match_parent"
					orientation="horizontal"
					gravity="center_vertical"
					background="#00ffffff"
					cornerRadius="0"
					accessibilityHint={HINT.HOMESCREEN_HEADER}
					style={this.style_Header}>
					<ImageView
						id={this.idSet.Scroll}
						height="48"
						width="16"
						orientation="vertical"
						gravity="center_horizontal"
						padding="0,0,0,4"
						imageUrl="scroll3"
						accessibilityHint={HINT.HOMESCREEN_SCROLL}
						style={this.style_Scroll} />
					<LinearLayout
						id={this.idSet.Space}
						weight="1"
						accessibilityHint={HINT.HOMESCREEN_SPACE}
						style={this.style_Space} />
					<ImageView
						id={this.idSet.Group_9}
						height="48"
						width="48"
						orientation="vertical"
						imageUrl="group94"
						accessibilityHint={HINT.HOMESCREEN_GROUP_9}
						style={this.style_Group_9} />
				</LinearLayout>
				<LinearLayout
					id={this.idSet.DashBoardGroup}
					height="248"
					width="match_parent"
					orientation="vertical"
					padding="16,13,16,16"
					margin="0,87,0,0"
					background="#ff202296"
					cornerRadius="0"
					accessibilityHint={HINT.HOMESCREEN_DASHBOARDGROUP}
					style={this.style_DashBoardGroup}>
					<LinearLayout
						id={this.idSet.HPCLPointsGroup}
						height="31"
						width="match_parent"
						orientation="horizontal"
						gravity="center_vertical"
						background="#00d7d7d7"
						cornerRadius="0"
						accessibilityHint={HINT.HOMESCREEN_HPCLPOINTSGROUP}
						style={this.style_HPCLPointsGroup}>
						<TextView
							id={this.idSet.PointLabelText}
							height="31"
							width="264"
							weight="1"
							textSize="24"
							color="#ffffffff"
							fontStyle="SourceSansPro-Regular"
							gravity="left"
							accessibilityHint={HINT.HOMESCREEN_POINTLABELTEXT}
							text={STR.HOMESCREEN_POINTLABELTEXT}
							style={this.style_PointLabelText} />
						<Ic_refresh_grey600
							parentProps={this.props_Refresh}
							height="24"
							width="24" />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.PointsGroup}
						height="80"
						width="184"
						orientation="horizontal"
						padding="23,0,0,0"
						background="#00d7d7d7"
						cornerRadius="0"
						accessibilityHint={HINT.HOMESCREEN_POINTSGROUP}
						style={this.style_PointsGroup}>
						<TextView
							id={this.idSet.PointsText}
							height="80"
							width="102"
							textSize="64"
							color="#ffffffff"
							fontStyle="SourceSansPro-Bold"
							gravity="left"
							accessibilityHint={HINT.HOMESCREEN_POINTSTEXT}
							text={STR.HOMESCREEN_POINTSTEXT}
							style={this.style_PointsText} />
						<TextView
							id={this.idSet.TotalText}
							height="40"
							width="59"
							margin="0,32,0,0"
							textSize="32"
							color="#7fffffff"
							fontStyle="SourceSansPro-Regular"
							gravity="left"
							accessibilityHint={HINT.HOMESCREEN_TOTALTEXT}
							text={STR.HOMESCREEN_TOTALTEXT}
							style={this.style_TotalText} />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.Space}
						weight="1"
						accessibilityHint={HINT.HOMESCREEN_SPACE}
						style={this.style_Space} />
					<LinearLayout
						id={this.idSet.LevelGroup}
						height="48"
						width="140"
						orientation="horizontal"
						background="#00d7d7d7"
						cornerRadius="0"
						accessibilityHint={HINT.HOMESCREEN_LEVELGROUP}
						style={this.style_LevelGroup}>
						<ImageView
							id={this.idSet.LevelIcon}
							height="48"
							width="48"
							orientation="vertical"
							imageUrl="levelicon5"
							accessibilityHint={HINT.HOMESCREEN_LEVELICON}
							style={this.style_LevelIcon} />
						<LinearLayout
							id={this.idSet.LevelTextGroup}
							height="35"
							width="82"
							orientation="vertical"
							gravity="center_horizontal"
							margin="10,8,0,0"
							background="#00d7d7d7"
							cornerRadius="0"
							accessibilityHint={HINT.HOMESCREEN_LEVELTEXTGROUP}
							style={this.style_LevelTextGroup}>
							<TextView
								id={this.idSet.LevelLabel}
								height="15"
								width="match_parent"
								textSize="12"
								color="#ffffffff"
								fontStyle="SourceSansPro-Regular"
								gravity="left"
								accessibilityHint={HINT.HOMESCREEN_LEVELLABEL}
								text={STR.HOMESCREEN_LEVELLABEL}
								style={this.style_LevelLabel} />
							<TextView
								id={this.idSet.LevelText}
								height="20"
								width="match_parent"
								textSize="16"
								color="#ffffffff"
								fontStyle="SourceSansPro-Regular"
								gravity="left"
								accessibilityHint={HINT.HOMESCREEN_LEVELTEXT}
								text={STR.HOMESCREEN_LEVELTEXT}
								style={this.style_LevelText} />
						</LinearLayout>
					</LinearLayout>
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Space}
					weight="1"
					accessibilityHint={HINT.HOMESCREEN_SPACE}
					style={this.style_Space} />
				<LinearLayout
					id={this.idSet.ViewStatGroup}
					height="32"
					width="163"
					orientation="horizontal"
					gravity="center_vertical"
					accessibilityHint={HINT.HOMESCREEN_VIEWSTATGROUP}
					style={this.style_ViewStatGroup}>
					<Ic_expand_more_grey600
						parentProps={this.props_Ic_expand_more_grey600}
						height="32"
						width="32" />
					<TextView
						id={this.idSet.View_Statement}
						height="16"
						width="126"
						margin="4,0,0,0"
						textSize="14"
						color="#ff333333"
						fontStyle="Roboto-Medium"
						gravity="left"
						accessibilityHint={HINT.HOMESCREEN_VIEW_STATEMENT}
						text={STR.HOMESCREEN_VIEW_STATEMENT}
						style={this.style_View_Statement} />
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = HomeScreen;
