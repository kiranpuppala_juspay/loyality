const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const HeaderWithArrow = require('../../components/HeaderWithArrow');
const TextView = require("@juspay/mystique-backend").views.TextView;
const Input = require('../../components/Input');
const PrimaryButton = require('../../components/PrimaryButton');

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/A_3a_Add_VPA_FilledSS');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class A_3a_Add_VPA_FilledSS extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				gravity="center_horizontal"
				padding="0,0,0,8"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.A_3A_ADD_VPA_FILLEDSS_GROUP}
				style={this.style_Group}>
				<HeaderWithArrow
					parentProps={this.props_HeaderNav}
					height="64"
					width="match_parent" />
				<TextView
					id={this.idSet.You_d_be_rewarded_po}
					height="44"
					width="match_parent"
					margin="20,10,20,0"
					textSize="16"
					color="#ff333333"
					fontStyle="SourceSansPro-Regular"
					lineHeight="22px"
					gravity="left"
					accessibilityHint={HINT.A_3A_ADD_VPA_FILLEDSS_YOU_D_BE_REWARDED_PO}
					text={STR.A_3A_ADD_VPA_FILLEDSS_YOU_D_BE_REWARDED_PO}
					style={this.style_You_d_be_rewarded_po} />
				<Input
					parentProps={this.props_Input}
					height="68"
					width="match_parent"
					margin="0,13,0,0"
					InputText={STR.A_3A_ADD_VPA_FILLEDSS_INPUT_INPUTTEXT} />
				<LinearLayout
					id={this.idSet.Group_4}
					height="20"
					width="match_parent"
					orientation="horizontal"
					gravity="center_vertical"
					padding="0,0,0,1"
					margin="20,20,20,0"
					background="#ffffffff"
					cornerRadius="0"
					accessibilityHint={HINT.A_3A_ADD_VPA_FILLEDSS_GROUP_4}
					style={this.style_Group_4}>
					<LinearLayout
						id={this.idSet.Group_2}
						height="12"
						width="12"
						orientation="vertical"
						accessibilityHint={HINT.A_3A_ADD_VPA_FILLEDSS_GROUP_2}
						style={this.style_Group_2}>
						<LinearLayout
							id={this.idSet.Combined_Shape}
							height="12"
							width="12"
							background="#ff1f2196"
							cornerRadius="0"
							accessibilityHint={HINT.A_3A_ADD_VPA_FILLEDSS_COMBINED_SHAPE}
							style={this.style_Combined_Shape} />
					</LinearLayout>
					<TextView
						id={this.idSet.Add_more_UPI_ID}
						height="20"
						width="288"
						margin="20,0,0,0"
						weight="1"
						textSize="16"
						color="#ff1f2196"
						fontStyle="SourceSansPro-Regular"
						gravity="left"
						accessibilityHint={HINT.A_3A_ADD_VPA_FILLEDSS_ADD_MORE_UPI_ID}
						text={STR.A_3A_ADD_VPA_FILLEDSS_ADD_MORE_UPI_ID}
						style={this.style_Add_more_UPI_ID} />
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Space}
					weight="1"
					accessibilityHint={HINT.A_3A_ADD_VPA_FILLEDSS_SPACE}
					style={this.style_Space} />
				<PrimaryButton
					parentProps={this.props_Button_Primary}
					height="48"
					width="match_parent"
					margin="8,0,8,0" />
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = A_3a_Add_VPA_FilledSS;
