const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const HeaderWithoutArrow = require('../../components/HeaderWithoutArrow');
const TextView = require("@juspay/mystique-backend").views.TextView;

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/OTPWaiting');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class OTPWaiting extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	WaitingGroup_flow = () => {
		window.__duiShowScreen(null, {screen: "OTPFilledNew"});
	}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				gravity="center_horizontal"
				padding="0,0,0,444"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.OTPWAITING_OTPWAITING}
				style={this.style_OTPWaiting}>
				<HeaderWithoutArrow
					parentProps={this.props_Header}
					height="64"
					width="match_parent"
					HeaderText={STR.OTPWAITING_HEADER_HEADERTEXT} />
				<LinearLayout
					id={this.idSet.OTPGroup}
					height="132"
					width="match_parent"
					orientation="vertical"
					padding="0,20,0,0"
					background="#ffffffff"
					cornerRadius="0"
					accessibilityHint={HINT.OTPWAITING_OTPGROUP}
					style={this.style_OTPGroup}>
					<TextView
						id={this.idSet.ParaText}
						height="22"
						width="match_parent"
						margin="20,0,20,0"
						textSize="16"
						color="#ff333333"
						fontStyle="SourceSansPro-Regular"
						lineHeight="22px"
						gravity="left"
						accessibilityHint={HINT.OTPWAITING_PARATEXT}
						text={STR.OTPWAITING_PARATEXT}
						style={this.style_ParaText} />
					<LinearLayout
						id={this.idSet.WaitingGroup}
						height="68"
						width="match_parent"
						orientation="vertical"
						padding="20,18,20,19"
						margin="0,20,0,0"
						background="#fff4f4fa"
						cornerRadius="0"
						onClick={this.WaitingGroup_flow}
						accessibilityHint={HINT.OTPWAITING_WAITINGGROUP}
						style={this.style_WaitingGroup}>
						<TextView
							id={this.idSet.WaitingText}
							height="31"
							width="320"
							textSize="24"
							color="#7f202296"
							fontStyle="SourceSansPro-Regular"
							gravity="center"
							accessibilityHint={HINT.OTPWAITING_WAITINGTEXT}
							text={STR.OTPWAITING_WAITINGTEXT}
							style={this.style_WaitingText} />
					</LinearLayout>
					<LinearLayout
						id={this.idSet.ProgressBar}
						height="2"
						width="250"
						background="#ff8081c3"
						cornerRadius="0"
						accessibilityHint={HINT.OTPWAITING_PROGRESSBAR}
						style={this.style_ProgressBar} />
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = OTPWaiting;
