const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const HeaderWithoutArrow = require('../../components/HeaderWithoutArrow');
const TextView = require("@juspay/mystique-backend").views.TextView;
const PrimaryButton = require('../../components/PrimaryButton');

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/A_2b1_OTP_FilledSS');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class A_2b1_OTP_FilledSS extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				gravity="center_horizontal"
				padding="0,0,0,8"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.A_2B1_OTP_FILLEDSS_GROUP_2}
				style={this.style_Group_2}>
				<HeaderWithoutArrow
					parentProps={this.props_Header}
					height="64"
					width="match_parent"
					HeaderText={STR.A_2B1_OTP_FILLEDSS_HEADER_HEADERTEXT} />
				<TextView
					id={this.idSet.Welcome_back_Harish_}
					height="44"
					width="match_parent"
					margin="20,10,20,0"
					textSize="16"
					color="#ff333333"
					fontStyle="SourceSansPro-Regular"
					lineHeight="22px"
					gravity="left"
					accessibilityHint={HINT.A_2B1_OTP_FILLEDSS_WELCOME_BACK_HARISH_}
					text={STR.A_2B1_OTP_FILLEDSS_WELCOME_BACK_HARISH_}
					style={this.style_Welcome_back_Harish_} />
				<LinearLayout
					id={this.idSet.Group}
					height="68"
					width="match_parent"
					orientation="vertical"
					padding="0,18,0,19"
					margin="0,20,0,0"
					background="#fff4f4fa"
					cornerRadius="0"
					accessibilityHint={HINT.A_2B1_OTP_FILLEDSS_GROUP}
					style={this.style_Group}>
					<TextView
						id={this.idSet.A_512398}
						height="31"
						width="360"
						textSize="24"
						color="#ff1f2196"
						fontStyle="SourceSansPro-Semibold"
						gravity="center"
						accessibilityHint={HINT.A_2B1_OTP_FILLEDSS_A_512398}
						text={STR.A_2B1_OTP_FILLEDSS_A_512398}
						style={this.style_A_512398} />
				</LinearLayout>
				<LinearLayout
					id={this.idSet.Space}
					weight="1"
					accessibilityHint={HINT.A_2B1_OTP_FILLEDSS_SPACE}
					style={this.style_Space} />
				<LinearLayout
					id={this.idSet.Group_3}
					height="48"
					width="match_parent"
					orientation="vertical"
					margin="8,0,8,0"
					accessibilityHint={HINT.A_2B1_OTP_FILLEDSS_GROUP_3}
					style={this.style_Group_3}>
					<LinearLayout
						id={this.idSet.Space}
						weight="1"
						accessibilityHint={HINT.A_2B1_OTP_FILLEDSS_SPACE}
						style={this.style_Space} />
					<PrimaryButton
						parentProps={this.props_Button_Primary}
						height="48"
						width="match_parent"
						margin="8,0,8,0" />
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = A_2b1_OTP_FilledSS;
