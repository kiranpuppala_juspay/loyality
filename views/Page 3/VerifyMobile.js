const View = require("@juspay/mystique-backend").baseView;
const dom = require("@juspay/mystique-backend").doms;

const LinearLayout = require("@juspay/mystique-backend").views.LinearLayout;
const HeaderWithoutArrow = require('../../components/HeaderWithoutArrow');
const ScrollView = require("@juspay/mystique-backend").views.ScrollView;
const TextView = require("@juspay/mystique-backend").views.TextView;
const Input = require('../../components/Input');
const PrimaryButton = require('../../components/PrimaryButton');

const Config = require('./../../globalConfig');
const Controller = require('./../../controller/pages/Page 3/VerifyMobile');
const Strings = require('./../../res/strings');
const Accessibility = require('./../../res/accessibility');

let STR = {};
let HINT = {};

class VerifyMobile extends Controller {

	constructor(props, children, state) {
		super(props, children, state);
		this.shouldCacheScreen = false;
		STR = Strings();
		HINT = Accessibility();
	}

	onPop = () => {}

	render = () => {
		this.layout = (
			<LinearLayout
				height="match_parent"
				width="match_parent"
				orientation="vertical"
				gravity="center_horizontal"
				background="#ffffffff"
				cornerRadius="0"
				root={true}
				accessibilityHint={HINT.VERIFYMOBILE_VERIFYMOBILE}
				style={this.style_VerifyMobile}>
				<HeaderWithoutArrow
					parentProps={this.props_Header}
					height="64"
					width="match_parent" />
				<ScrollView
					id={this.idSet.VerifyScrollView}
					height="0"
					width="match_parent"
					weight="1"
					background="#ffffffff"
					cornerRadius="0"
					accessibilityHint={HINT.VERIFYMOBILE_VERIFYSCROLLVIEW}
					style={this.style_VerifyScrollView}>
					<LinearLayout
						id={this.idSet.VerifyGroup}
						height="182"
						width="match_parent"
						orientation="vertical"
						gravity="center_horizontal"
						padding="0,20,0,0"
						background="#ffffffff"
						cornerRadius="0"
						accessibilityHint={HINT.VERIFYMOBILE_VERIFYGROUP}
						style={this.style_VerifyGroup}>
						<TextView
							id={this.idSet.Welcome_to_HPCL_Payb}
							height="44"
							width="match_parent"
							margin="20,0,20,0"
							textSize="16"
							color="#ff333333"
							fontStyle="SourceSansPro-Regular"
							lineHeight="22px"
							gravity="left"
							accessibilityHint={HINT.VERIFYMOBILE_WELCOME_TO_HPCL_PAYB}
							text={STR.VERIFYMOBILE_WELCOME_TO_HPCL_PAYB}
							style={this.style_Welcome_to_HPCL_Payb} />
						<Input
							parentProps={this.props_Input}
							height="68"
							width="match_parent"
							margin="0,20,0,0" />
						<LinearLayout
							id={this.idSet.Group_3}
							height="24"
							width="match_parent"
							orientation="horizontal"
							gravity="center_vertical"
							padding="0,0,8,0"
							margin="20,6,20,0"
							background="#ffffffff"
							cornerRadius="0"
							accessibilityHint={HINT.VERIFYMOBILE_GROUP_3}
							style={this.style_Group_3}>
							<LinearLayout
								id={this.idSet.Rectangle_9}
								height="24"
								width="2"
								background="#ffd7d7d7"
								cornerRadius="0"
								accessibilityHint={HINT.VERIFYMOBILE_RECTANGLE_9}
								style={this.style_Rectangle_9} />
							<TextView
								id={this.idSet.This_will_be_linked}
								height="15"
								width="300"
								margin="10,0,0,0"
								weight="1"
								textSize="12"
								color="#ff333333"
								fontStyle="SourceSansPro-Regular"
								gravity="left"
								accessibilityHint={HINT.VERIFYMOBILE_THIS_WILL_BE_LINKED}
								text={STR.VERIFYMOBILE_THIS_WILL_BE_LINKED}
								style={this.style_This_will_be_linked} />
						</LinearLayout>
					</LinearLayout>
				</ScrollView>
				<LinearLayout
					id={this.idSet.ButtonGroup}
					height="64"
					width="match_parent"
					orientation="vertical"
					padding="8,8,8,8"
					background="#ffffffff"
					cornerRadius="0"
					accessibilityHint={HINT.VERIFYMOBILE_BUTTONGROUP}
					style={this.style_ButtonGroup}>
					<PrimaryButton
						parentProps={this.props_PrimaryButton}
						height="48"
						width="match_parent"
						ButtonText={STR.VERIFYMOBILE_PRIMARYBUTTON_BUTTONTEXT} />
				</LinearLayout>
			</LinearLayout>
		);
		return this.layout.render();
	}

};

module.exports = VerifyMobile;
