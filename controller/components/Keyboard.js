const View = require("@juspay/mystique-backend").baseView;

class Keyboard extends View {

	constructor(props, children, state) {
		super(props, children);

		let pressed = {
			onClickNumber: this.handle.bind(this)
		}

		this.props_A_1 = pressed;
		this.props_A_2 = pressed;
		this.props_A_3 = pressed;
		this.props_A_4 = pressed;
		this.props_A_5 = pressed;
		this.props_A_6 = pressed;
		this.props_A_7 = pressed;
		this.props_A_8 = pressed;
		this.props_A_9 = pressed;
		this.props_A_0 = pressed;

		this.style_BackSpace = {
			onClick: this.backSpace.bind(this)
		}

		this.style_Verify = {
			onClick: this.verify.bind(this)
		}

	}

	handle(num) {
		this.props.parentProps.onClickKey(num);
	}

	backSpace() {
		this.props.parentProps.onClickBackSpace();
	}

	verify(){
		this.props.parentProps.onClickVerify();

	}

}

module.exports = Keyboard;
