const View = require("@juspay/mystique-backend").baseView;

class Button_Raised extends View {

	constructor(props, children, state) {
		super(props, children);
	}

}

module.exports = Button_Raised;
