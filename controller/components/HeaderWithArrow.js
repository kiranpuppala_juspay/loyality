const View = require("@juspay/mystique-backend").baseView;

class HeaderWithArrow extends View {

	constructor(props, children, state) {
		super(props, children);

		this.style_Icon = {
			onClick: this.handle.bind(this),
			padding: "0,0,0,0"
		}
	}

	handle() {
		this.props.parentProps.onClickService();
	}

}

module.exports = HeaderWithArrow;
