const View = require("@juspay/mystique-backend").baseView;

class HeaderWithoutArrow extends View {

	constructor(props, children, state) {
		super(props, children);

		let match_parent = {
			width: "match_parent"
		}

		this.style_HeaderText = match_parent;
	}

}

module.exports = HeaderWithoutArrow;
