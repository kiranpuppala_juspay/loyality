const View = require("@juspay/mystique-backend").baseView;

class PrimaryButton extends View {

	constructor(props, children, state) {
		super(props, children);

		let match_parent = {
			width: "match_parent"
		}

		this.style_ButtonGroup = {
			onClick: this.handle.bind(this)
		}

		this.style_ButtonText = match_parent;
	}

	handle() {
		this.props.parentProps.onClickService();
	}

}

module.exports = PrimaryButton;
