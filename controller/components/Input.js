const View = require("@juspay/mystique-backend").baseView;

class Input extends View {

	constructor(props, children, state) {
		super(props, children);

		this.style_InputText =  {
			inputType : this.props.parentProps.inputType,
			onChange: this.handle.bind(this)
		}
	}

	handle(text) {
		this.props.parentProps.onChangeMob(text);
	}

}

module.exports = Input;
