const View = require("@juspay/mystique-backend").baseView;

class Ic_expand_more_grey600 extends View {

	constructor(props, children, state) {
		super(props, children);

		this.style_Ic_expand_more_24px = {
			padding: "0,0,0,0"
		}
	}

}

module.exports = Ic_expand_more_grey600;
