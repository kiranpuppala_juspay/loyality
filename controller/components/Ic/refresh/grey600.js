const View = require("@juspay/mystique-backend").baseView;

class Ic_refresh_grey600 extends View {

	constructor(props, children, state) {
		super(props, children);
	}

}

module.exports = Ic_refresh_grey600;
