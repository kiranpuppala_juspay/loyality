const View = require("@juspay/mystique-backend").baseView;

class Number extends View {

	constructor(props, children, state) {
		super(props, children);

		this.style_Number = {
			onClick: this.handle.bind(this)
		}

		this.style_Text = {
			width: "match_parent"
		}
	}

	handle() {
		let number = this.props.Text;
		if (!number)
			number = this.Text;
		this.props.parentProps.onClickNumber(number);
	}
}

module.exports = Number;
