const View = require("@juspay/mystique-backend").baseView;
const animations = require("@juspay/mystique-backend").animations.types;


class Transactions extends View {

	constructor(props, children, state) {
		super(props, children);

		this.screenTransition = animations.SLIDE_VERTICAL;

		let match_parent = {
			width: "match_parent"
		}

		this.style_ScrollGroup = match_parent;
		
		this.style_ToHowToGroup = {
			onClick: this.onClickRedeem.bind(this)
		}

		this.style_ToHomeGroup = {
			onClick: this.onClickHome.bind(this)
		}

		this.style_Group_9 = {
			onClick: this.onClickIcon.bind(this)
		}

		this.style_DetailedIcon = {
			padding: "0,0,0,0"
		}
	}

	onClickRedeem() {
		window.__duiShowScreen(null,{screen: "RedeemScreen"});
	}

	onClickHome() {
		window.__duiShowScreen(null,{screen: "HomeScreen"});
	}

	onClickIcon() {
		window.__duiShowScreen(null,{screen: "VerifyMobile"});

	}

}

module.exports = Transactions;
