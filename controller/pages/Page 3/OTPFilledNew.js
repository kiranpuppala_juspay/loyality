const View = require("@juspay/mystique-backend").baseView;

class OTPFilledNew extends View {

	constructor(props, children, state) {
		super(props, children);

		let match_parent = {
			width: "match_parent"
		}

		this.style_OTPFilledGroup = match_parent;

		this.style_OTPText = match_parent;
		
		this.props_PrimaryButton = {
			onClickService: this.onClickButton.bind(this)
		}
	}

	onClickButton() {
		window.__duiShowScreen(null,{screen: "AddVPA"});
	}

}

module.exports = OTPFilledNew;
