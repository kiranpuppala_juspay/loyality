const View = require("@juspay/mystique-backend").baseView;

class HomeScreen extends View {

	constructor(props, children, state) {
		super(props, children);

		this.style_TotalText = {
			margin: "0,0,0,0"
		}

		this.style_Group_9 = {
			onClick: this.onClickIcon.bind(this)
		}
	
		this.style_ViewStatGroup = {
			onClick: this.onClickButton.bind(this)
		}
	}

	onClickButton() {
		window.__duiShowScreen(null,{screen: "Transactions"});
	}

	onClickIcon() {
		window.__duiShowScreen(null,{screen: "VerifyMobile"});

	}

}

module.exports = HomeScreen;
