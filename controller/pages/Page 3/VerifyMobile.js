const View = require("@juspay/mystique-backend").baseView;
const axios = require('axios');

class VerifyMobile extends View {

	constructor(props, children, state) {
		super(props, children);

		let match_parent = {
			width: "match_parent"
		}

		this.props_Input = {
			inputType: "numeric",
			onChangeMob: this.mobileNo.bind(this)
		}

		this.props_PrimaryButton = {
			onClickService: this.onClickButton.bind(this)
		}

		this.style_VerifyGroup = match_parent;
	}

	onClickButton() {
		this.sendOTP();
		window.__duiShowScreen(null,{screen: "OTPWaiting", otp: this.otp});
	}

	mobileNo(text){
		this.mobileNo = text;
	}

	sendOTP() {
		this.otp = Math.floor(Math.random() * (999999 - 100000)) + 100000;
		var text = "Dear Customer, "+this.otp+" is your one time password (OTP). Please enter the OTP to proceed. Thank you, Team JusPay";
		var url = "http://bhashsms.com/api/sendmsg.php?phone="+this.mobileNo+"&text="+text+"&pass=squarebo11&user=juspay&sender=JUSPAY&priority=ndnd";

		axios.get(url)
			.then(function (response) {
		    	console.log(response);
			})
		  	.catch(function (error) {
		    	console.log(error);
		  	});
	}

}

module.exports = VerifyMobile;
