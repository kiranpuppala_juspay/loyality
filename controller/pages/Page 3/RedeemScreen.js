const View = require("@juspay/mystique-backend").baseView;
const animations = require("@juspay/mystique-backend").animations.types;


class RedeemScreen extends View {

	constructor(props, children, state) {
		super(props, children);

		this.screenTransition = animations.SLIDE_VERTICAL;
		
		
		this.style_ViewStatGroup = {
			onClick: this.onClickView.bind(this)
		}

		this.style_Group_9 = {
			onClick: this.onClickIcon.bind(this)
		}
	}

	onClickView() {
		window.__duiShowScreen(null,{screen: "Transactions"});
	}

	onClickIcon() {
		window.__duiShowScreen(null,{screen: "VerifyMobile"});

	}

}

module.exports = RedeemScreen;
