const View = require("@juspay/mystique-backend").baseView;

var otp = "______";
var count = 0;

class OTPWaiting extends View {

	constructor(props, children, state) {
		super(props, children, state);

		this.state = state;
		console.log(this.state.otp);

		this.setIds(['WaitingText']);

		this.style_WaitingText = {
			width: "match_parent",
			letterSpacing: "1"
		}

		this.style_WaitingGroup = {
			// onClickService: this.onClickButton.bind(this)
		}

		this.props_Keyboard = {
			onClickKey: this.onClickKeyBoard.bind(this),
			onClickBackSpace: this.onClickBackSpace.bind(this),
			onClickVerify: this.onClickVerify.bind(this)
		}
	}

	onClickKeyBoard(num){
		if(count >=0 && count <=5) {
			otp = otp.substr(0, count) + num + otp.substr(count + 1);
			var cmd = this.cmd({id: this.idSet.WaitingText, text:otp});
			Android.runInUI(cmd,null);
			count++;
		}
	}

	onClickBackSpace() {
		if(count >=0 && count <=5){
			count--;
			otp = otp.substr(0, count) + "_" + otp.substr(count + 1);
			var cmd = this.cmd({id: this.idSet.WaitingText, text:otp});
			Android.runInUI(cmd,null);
		}
	}

	onClickVerify() {
		if(this.state.otp == parseInt(otp))
			window.__duiShowScreen(null,{screen: "OTPFilledNew"});	
	}
}

module.exports = OTPWaiting;
