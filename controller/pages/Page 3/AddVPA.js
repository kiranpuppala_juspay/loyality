const View = require("@juspay/mystique-backend").baseView;

class AddVPA extends View {

	constructor(props, children, state) {
		super(props, children);

		let match_parent = {
			width: "match_parent"
		}

		this.props_Input = {
			inputType: "text"
		}

		this.props_HeaderWithArrow = {
			onClickService: this.onClickBack.bind(this)
		}

		this.style_AddVPAGroup = match_parent;
		
		this.props_PrimaryButton = {
			onClickService: this.onClickButton.bind(this)
		}
	}

	onClickBack() {
		window.__duiShowScreen(null,{screen: "VerifyMobile"});

	}

	onClickButton() {
		window.__duiShowScreen(null,{screen: "HomeScreen"});
	}

}

module.exports = AddVPA;
